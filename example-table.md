| Ansible Community Package Releas | Status | Core version dependency |
| ----------- | ----------- | :-----------: |
| 7.0.0      | In development (unreleased)       | 2.14 |
| 6.x   | Current        | 2.13 |
| 5.x   | Unmaintained (end of life)        | 2.12 |
| 4.x   | Unmaintained (end of life)        | 2.11 |
| 3.x   | Unmaintained (end of life)        | 2.10 |
| 2.10   | Unmaintained (end of life)        | 2.10 |


Server spek 500GB SSD + 1 TB HDD
| Disk | Ukuran | partisi | Mount Point |

| --- | --- | :---: |
| SSD 500G | 1024M | /dev/sda1 | /boot |
| ~ | 192GB | /dev/sda2 | / |
| ~ | 307GB | /dev/sda2 | /data/vms |
| HDD 1000GB(1TB) | 16GB | /dev/sdb1 | swap |
| HDD 1000GB(1TB) | GB | /dev/sdb2 | /data/isos |

Server spek 500GB
